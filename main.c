#include <stdio.h>
#include <math.h>

int main() {
//calculating the root of the equation
 printf("finding the roots of 'ax^2 + bx + c'\n");
 double a;
 double b;
 double c;
 //print and scan the values of a, b, c
 printf("enter the value of a\n");
 scanf("%lf",&a);
 printf("enter the value of b\n");
 scanf("%lf",&b);
 printf("enter the value of c\n");
 scanf("%lf",&c);
 if (a == 0){
    //validating the value of a
    printf("The value of a cannot be zero");
    return 1;
}
 else {
     //calculating the value of the discriminant
     double discriminant = b*b - 4*a*c;
     if (discriminant > 0){
        printf("distinct root\n");
        double root1 = (-b + sqrt(discriminant))/2*a;
        double root2 = (-b - sqrt(discriminant))/2*a;
        printf("root1 = %lf\n,root1");
        printf("root2 = %lf\n,root2");
     }
     else if (discriminant = 0) {
        printf("one real root\n");
        double root = -b/2*a;
        printf("root = %lf\n", root);
     }
     else{
        printf("complex root");
     }
}
    return 0;
}
